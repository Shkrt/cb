#
# Cookbook:: rails_app
# Recipe:: default
#
# Copyright:: 2021, The Authors, All Rights Reserved.

node.default['packages-cookbook'] = [
 'nodejs',
 'imagemagick',
 'gcc72',
 'gcc72-c++',
 'htop'
 'tmux'
]
